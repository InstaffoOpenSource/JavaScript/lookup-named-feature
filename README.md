﻿
<!--#echo json="package.json" key="name" underline="=" -->
@instaffogmbh/lookup-named-feature
==================================
<!--/#echo -->

<!--#echo json="package.json" key="description" -->
Dictionary lookup function with indirection and promise support.
<!--/#echo -->

* 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).



API
---

This module exports one function:

### feature = lookupNamedFeature(features, key[, opt])

`opt` is an optional options object.
With no options, LNF returns a refined `features[key]`, where
"refined" means:
* In case of a promise, wrap it in a promise that annotates the rejection
  reason with info about where the original promise was found.

Supported options:
* `cfg`: If set, should be a dictionary object.
  Look up the real `key` here first. (`features[opt.cfg[key]]`)

Options supported for non-Promises:
* `dive`: Dive deeper into whatever was found.
  Should be a path compatible with `lodash.get`.
* `call`: If set to a `true`-y value, expect to find a function,
  invoke it, and return its result.
  Should be either `true` for no function arguments, or an array of
  values to be used as function arguments. (The array may be empty.)
* `wantJsType`: If `true`-y, compare with `typeof` whatever was found
  and complain if it differs.




<!--#toc stop="scan" -->


&nbsp;


License
-------

<!--#echo json="package.json" key=".license" -->
MIT
<!--/#echo -->
